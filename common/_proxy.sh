#!/usr/bin/env bash

# Proxy
#
# This script will source `common/<script-name>` first.
# Then it will look for the script in a specific directory for a language.
# Depending on environment variable 'LANGUAGE'.
# If no script found in the directory, it will pass it to generic/<script-name>
#

cd `dirname "$0"`
cd ..

file=$(basename $1)
echo "Looking for a special version of $file"

shift

language=${LANGUAGE:-common}
echo "Language: $language"


filename="common/$file"
if [ -f $filename ]; then
  echo "file ($filename) exists"
  source $filename
else
  echo "file ($filename) does not exist"
fi

filename="$language/$file"
if [ -f $filename ]; then
  echo "file ($filename) exists"
  source $filename
else
  echo "file ($filename) does not exist"
  filename="generic/$file"
  if [ -f $filename ]; then
    echo "file ($filename) exists"
    source $filename
  else
    echo "file ($filename) does not exist"
    exit 1
  fi
fi

