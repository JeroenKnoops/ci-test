# ci-test

## Purpose

Test to make a proxy to language specific build files.

## Usage

See [build.sh](./build.sh) for example.

```
#!/usr/bin/env bash

echo "Build"

./common/_proxy.sh $0 "$@"
```

### What is the function of `_proxy.sh`.

The `_proxy.sh` script will look (and run) the following scripts:

It will check the `common` directory for a script with the same name and it will run it.
Depending on env variable: LANGUAGE it will check for a script with the same name and will run it.
If no file found it will run it from `generic` if found.

## Content

```
.
├── build.sh
├── check.sh
├── common
│   ├── _proxy.sh
│   ├── build.sh
│   └── check.sh
├── generic
│   ├── build.sh
│   └── check.sh
├── java
│   └── check.sh
└── python
    ├── build.sh
    └── check.sh
```

### Examples

#### Common and specific scripts are available

``` bash
export LANGUAGE=java
./check.sh "param1 is present" param2
```

- First `common/check.sh` is executed with both parameters.
- Then `java/check.sh` is executed with both parameters.

#### Common and generic scripts are available

``` bash
export LANGUAGE=java
./build.sh "param1 is present" param2
```

- First `common/build.sh` is executed with both parameters.
- Then `generic/build.sh` is executed with both parameters..
